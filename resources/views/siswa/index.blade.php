<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Data Siswa</title>

    

  </head>
  <body>
    <!-- ini navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
    <a class="navbar-brand" href="#">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Apple_logo_white.svg/1200px-Apple_logo_white.svg.png" width="30" height="30" class="d-inline-block align-top" alt="">
    </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">ReXenSOFT</a>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="{{ url ('/') }}">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url ('/about') }}">About</a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="{{ url ('/siswa') }}">Data Siswa</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
    <!-- ini batas navbar -->
<br>
<br>
   <div class="container">
   @if(session('sukses'))
        <div class="alert alert-success" role="alert">
        {{session('sukses')}}
      </div>
    @endif

    <div class="jumbotron jumbotron-fluid">
      <div class="container">
        <div class="row">
        <!-- grid col data siswa -->
            <div class="col-12">
                <h1 style="padding: 0 14px;">Data Siswa <a href="/tambah" class="btn btn-info btn-md float-right text-light" role="button" >Tambah Data Siswa</a></h1>
            </div>
       </div>
            <!-- table ke database -->
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <table class="table table-bordered table-hover">
                  <thead class="thead-dark">
                      <tr class="text-align-center">
                          <th>Nama Lengkap Siswa</th>
                          <th>Jenis Kelamin</th>
                          <th>Agama</th>
                          <th>Alamat Tempat Tinggal</th>
                          <th class="align-center">Opsi</th>
                      </tr>
                      @foreach($data_siswa as $siswa)
                      <tr class="text-align-center">
                          <td>{{$siswa->nama_lengkap}}</td>
                          <td>{{$siswa->jenis_kelamin}}</td>
                          <td>{{$siswa->agama}}</td>
                          <td>{{$siswa->alamat}}</td>
                          <td style="display: flex; justify-content: space-between; align-items: center; width='10px'">
                            <a href="/siswa/{{$siswa->id}}/edit" class=" btn btn-warning btn-sm"> Edit Data </a>
                            <a href="/siswa/{{$siswa->id}}/delete" class=" btn btn-danger btn-sm" onclick="return confirm ('Yakin Dihapus?')" > Delete </a>
                          </td>
                      </tr>
                      @endforeach
                  </table>
                </div>
              </div>
            </div>

      </div>
    </div>

            <!-- akhir table ke database -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

<!--Java Script  -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Akhir JavaScript -->
  </body>
</html>