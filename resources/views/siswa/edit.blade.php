<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Data Siswa</title>
  </head>
  <body>
    <!-- ini navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
    <a class="navbar-brand" href="#">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Apple_logo_white.svg/1200px-Apple_logo_white.svg.png" width="30" height="30" class="d-inline-block align-top" alt="">
    </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">ReXenSOFT</a>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="{{ url ('/') }}">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url ('/about') }}">About</a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="{{ url ('/siswa') }}">Data Siswa</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
    <!-- ini batas navbar -->
<br>
<!-- alert -->
   <div class="container">
     <h1>Edit Data Siswa</h1>
         @if(session('sukses'))
            <div class="alert alert-success" role="alert">
            {{session('sukses')}}
            </div>
         @endif
    </div>
<!-- alert -->
<br>
    <!-- bawah ni adalah form -->
        <div class="row">
            <div class="container">
                <form action="/siswa/{{$siswa->id}}/update" method="POST">
                         {{csrf_field()}}
                          <div class="form-group" >
                            <label for="exampleFormControlInput1">Nama Lengkap</label>
                            <input name="nama_lengkap" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Isi Nama Lengkap" value="{{$siswa->nama_lengkap}}">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                            <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1" value="{{$siswa->jenis_kelamin}}">
                              <option value="Laki-Laki / (L)" @if($siswa->jenis_kelamin == 'L' ) selected @endif>Laki-Laki</option>
                              <option value="Perempuan / (P)" @if($siswa->jenis_kelamin == 'P' ) selected @endif>Perempuan</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Agama</label>
                            <select name="agama" class="form-control" id="exampleFormControlSelect1" value="{{$siswa->agama}}">
                              <option @if($siswa->agama == 'Islam' ) selected @endif>Islam</option>
                              <option @if($siswa->agama == 'Kristen' ) selected @endif>Kristen</option>
                              <option @if($siswa->agama == 'Katholik' ) selected @endif>Katholik</option>
                              <option @if($siswa->agama == 'Hindu' ) selected @endif>Hindu</option>
                              <option @if($siswa->agama == 'Budha' ) selected @endif>Budha</option>
                              <option @if($siswa->agama == 'Konghucu' ) selected @endif>Konghucu</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlTextarea1">Alamat</label>
                            <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="2" placeholder="Alamat Anda">{{$siswa->alamat}}</textarea>
                            <br>
                          <button type="submit" class="btn btn-success float-right">Update</button>
                          <a href="/siswa" class="btn btn-secondary btn-md active" role="button" aria-pressed="true">Kembali</a>
                    </form>
                </div>
              </div>
    <!-- atas ni adalah form -->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

<!--Java Script  -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Akhir JavaScript -->
  </body>
</html>