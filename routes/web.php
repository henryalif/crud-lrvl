<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::GET('/siswa', 'SiswaController@index');

Route::get('/about', 'SiswaController@about');

route::get('/siswa/{id}/edit', 'SiswaController@edit');

route::POST('/siswa/{id}/update', 'SiswaController@update');

route::POST('/siswa/create','SiswaController@create');

route::GET('/tambah','SiswaController@tambah');

route::GET('/siswa/{id}/delete', 'SiswaController@delete');